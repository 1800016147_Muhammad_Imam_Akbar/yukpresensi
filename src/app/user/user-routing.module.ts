import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserPage } from './user.page';

const routes: Routes = [
  {
    path: '',
    component: UserPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../user/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'rekap',
        loadChildren: () => import('../user/rekap/rekap.module').then( m => m.RekapPageModule)
      },
      {
        path: 'jadwal',
        loadChildren: () => import('../user/jadwal/jadwal.module').then( m => m.JadwalPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../user/profile/profile.module').then(m => m.ProfilePageModule)
      },
      {
        path: '',
        redirectTo: '/user/home',
        pathMatch: 'full'
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserPageRoutingModule { }
