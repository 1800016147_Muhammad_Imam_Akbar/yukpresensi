// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDAX94FLuXQfoVUA7U89Xd70CBpRU6aL5w",
    authDomain: "sistem-presensi-siswa.firebaseapp.com",
    projectId: "sistem-presensi-siswa",
    storageBucket: "sistem-presensi-siswa.appspot.com",
    messagingSenderId: "687568648670",
    appId: "1:687568648670:web:46714b297eed9bcd1d854f"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
